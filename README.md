# Différents outils pour calculer et apprendre à calculer en binaire et en hexadécimal sans calculatrice :

- Un tableau pour poser ses 1 et ses 0 afin de faciliter la conversion binaire-décimal ;
- Les tables d'addition et de soustraction des huit premières puissances de 2 ;
- Les tables de multiplication des dix premières puissances de 16.